﻿using System;

using asteroids.Types;

namespace asteroids.Models
{
    public class RandomRock
    {
        public RockSizes size;
        public Uri image;
        public int width;
    }
}
